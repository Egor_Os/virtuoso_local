#!/usr/bin/env python

from SPARQLWrapper import SPARQLWrapper, JSON

# connection_params = {
#     'hostname': '127.0.0.1',
#     'port': '8890',
#     'username': 'dba',
#     'password': 'dba'
# }

def run_query(sparql, query, silent=False):
    # not generalized at all
    sparql.setQuery(query)
    result = sparql.query().convert()
    if silent:
        return
    return result['results']['bindings'][0]

HASH_SYMBOL = '#'

sparql = SPARQLWrapper('http://localhost:8890/sparql')
sparql.setReturnFormat(JSON)
# sparql.setQuery(query)
# result = sparql.query().convert()
# print result['results']['bindings'][0]['yearOfPublication']['value']

print 'Creating GRAPH'
query = """ 
CREATE GRAPH <my_graph>;
"""
try:
    run_query(sparql, query, silent=True)
except:
    print '... already created'

print 'Inserting DATA'
query = """
INSERT DATA
  { 
    GRAPH <my_graph> 
      { 
        </book1> </price> 42 .
        </book1> </title> 'Crime and punishment'.
        </book1> </author> 'Dostaevky'.
        </book1> </year> 1866 .
        </book2> </price> 90 .
        </book2> </title> 'Journey from St. Petersburg to Moscow'.
        </book2> </author> 'Radishev'.
        </book2> </year> 1790.


      } 
  }"""
run_query(sparql, query, silent=True)

print 'Selecting DATA'
query = """
SELECT ?yearOfPublication
WHERE {
    ?o </title> 'Crime and punishment' ;
       </year> ?yearOfPublication .
}
""" 
output = run_query(sparql, query)
print output['yearOfPublication']['value']