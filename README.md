# Jenkins essentials

## Default environmental variables

http://localhost:8080/env-vars.html/

# SPARQL and Virtuoso essentials

## Virtuoso container
Spin up [Virtuoso](https://hub.docker.com/r/tenforce/virtuoso/) container

```sudo docker-compose -p virtuoso_test up -d #to start```
```sudo docker-compose -p virtuoso_test up -d #to stop```
```sudo docker exec -it virtuoso_test_db_1 bash #to enter container terminal```
**virtuoso_test** comes from prefix -p, **db** - from docker-compose.yml and **1** is an index

## SPARQL

While in Virtuoso terminal run:

```isql-v -U {username} -P {password}```

Create graph

```
SPARQL 
CREATE GRAPH <my_graph>;
```

Insert triples
```
SPARQL
INSERT DATA
  { 
    GRAPH <my_graph> 
      { 
        <#book1> <#price> 42 .
        <#book1> <#title> 'Crime and punishment'.
        <#book1> <#author> 'Dostaevky'.
        <#book1> <#year> 1866 .
        <#book2> <#price> 90 .
        <#book2> <#title> 'Journey from St. Petersburg to Moscow'.
        <#book2> <#author> 'Radishev'.
        <#book2> <#year> 1790.


      } 
  };
```

Query #1

```
SPARQL
SELECT ?cls
WHERE {?o <#title> ?cls};
```

Query #2.1 - year of publication by book title

```
SPARQL
SELECT ?year
WHERE {
    ?o <#title> 'Crime and punishment' .
    ?o <#year>  ?year .
};
```

Query #2.2 - year of publication by book title shortened

```
SPARQL
SELECT ?year
WHERE {
    ?o <#title> 'Crime and punishment' ;
        <#year>  ?year .
};
```
